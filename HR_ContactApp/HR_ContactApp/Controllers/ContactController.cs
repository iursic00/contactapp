﻿using HR_ContactApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HR_ContactApp.Controllers
{
    public class ContactController : Controller
    {
        private ContactsEntities db = new ContactsEntities();

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetContacts()
        {
            List<Person> temp = null;

            temp = db.Person.OrderBy(a => a.surname).ToList();

            var result = temp.Select(x => new Person { personID = x.personID, name = x.name, surname = x.surname, nickname = x.nickname, isBookmarked = x.isBookmarked });

            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public void UpdateBookmark(int id)
        {
            Person user = new Person();

            user = db.Person.Where(a => a.personID == id).FirstOrDefault();
            if (user != null)
            {
                if (user.isBookmarked == true)
                {
                    user.isBookmarked = false;
                }
                else
                {
                    user.isBookmarked = true;
                }
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public string insertPerson(Person contact, List<Phone> numbers, List<Email> emails, List<Tag> tags)
        {
            string message;


            
            if (emails != null)
            {
                List<Email> tempEmail = new List<Email>(emails);
                if (tempEmail[0].emailAdress != null)
                {
                    foreach (var email in tempEmail)
                    {
                        var query = (from p in db.Email
                                     where p.emailAdress == email.emailAdress
                                     select p.emailAdress).FirstOrDefault();
                        if (query == null)
                        {
                            email.personID = contact.personID;
                            db.Email.Add(email);
                        }
                        else
                            return "ABORTED!!! Email adresa: " + email.emailAdress + "  existing";
                    }
                }
            }
            if (numbers != null)
            {
                List<Phone> tempNumber = new List<Phone>(numbers);
                if (tempNumber[0].phoneNumber != null)
                {
                    foreach (var number in tempNumber)
                    {
                        var query = (from p in db.Phone
                                     where p.phoneNumber == number.phoneNumber
                                     select p.phoneNumber).FirstOrDefault();
                        if (query == null)
                        {
                            number.personID = contact.personID;
                            db.Phone.Add(number);
                        }
                        else
                            return "ABORTED!!! Phone number" + number.phoneNumber + " is existing";
                    }
                }
            }
            if (tags != null)
            {
                List<Tag> tempTags = new List<Tag>(tags);
                if (tempTags[0].nameTag != null)
                {
                    tempTags.ForEach(tag =>
                    {
                        tag.personID = contact.personID;
                        db.Tag.Add(tag);
                    });
                }              

            }
            if (contact.name != null)
            {
                db.Person.Add(contact);
                db.SaveChanges();

                message = "succesfuly";
            }
            else
            {
                message = "error";
            }

            return message;

        }
        public string deleteContact(int id)
        {
            string message;

            List<Person> tempPerson = null;

            if (id >= 0)
            {

                tempPerson = db.Person.Where(a => a.personID == id).ToList();
                tempPerson.ForEach(a => db.Person.Remove(a));
                db.SaveChanges();
                message = "Uspjesno";
            }
            else
            {
                message = "Neuspjesno";
            }
            return message;
        }

        public JsonResult getSearchResult(int option, string searchValue)
        {
            List<Person> temp = new List<Person>();
            List<Tag> tempTags = new List<Tag>();
            switch (option)
            {
                case 0:
                    temp = db.Person.Where(a => a.name.Contains(searchValue)).ToList();

                    break;
                case 1:
                    temp = db.Person.Where(a => a.surname.Contains(searchValue)).ToList();

                    break;
                case 2:
                    var tempR = (from p in db.Tag
                                 where p.nameTag == searchValue
                                 select p.personID).ToList();
                    tempR.ForEach(x =>
                    {
                        var tempList = db.Person.Where(a => a.personID == x).ToList();
                        temp.AddRange(tempList);
                    });

                    break;

                default:

                    return Json("error", JsonRequestBehavior.AllowGet);
            }           

            var searchResult = temp.Select(a => new Person
            {
                personID = a.personID,
                name = a.name,
                surname = a.surname,
                nickname = a.nickname,
                city = a.city,
                isBookmarked = a.isBookmarked

            });

            return Json(searchResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getBookmarked()
        {
            List<Person> temp = null;

            temp = db.Person.Where(a => a.isBookmarked == true).ToList();
            var returnResult = temp.Select(a => new Person { personID = a.personID, name = a.name, surname = a.surname, isBookmarked = a.isBookmarked });

            return Json(returnResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult editPerson(int id)
        {
            List<Person> tempPerson = new List<Person>();

            tempPerson = db.Person.Where(a => a.personID == id).ToList();

            var temp = tempPerson.Select(a => new Person
            {
                personID = a.personID,
                name = a.name,
                surname = a.surname,
                nickname = a.nickname,
                adress = a.adress,
                city = a.city,
                country = a.country
            });


            return Json(temp, JsonRequestBehavior.AllowGet);
        }

        public JsonResult editEmails(int id)
        {
            List<Email> tempEmail = new List<Email>();


            tempEmail = db.Email.Where(a => a.personID == id).ToList();

            var temp = tempEmail.Select(a => new Email
            {
                emailAdress = a.emailAdress
            });


            return Json(temp, JsonRequestBehavior.AllowGet);
        }

        public JsonResult editNumbers(int id)
        {
            List<Phone> tempPhone = new List<Phone>();

            tempPhone = db.Phone.Where(a => a.personID == id).ToList();
            var temp = tempPhone.Select(a => new Phone
            {
                phoneNumber = a.phoneNumber
            });


            return Json(temp, JsonRequestBehavior.AllowGet);
        }

        public JsonResult editTags(int id)
        {
            List<Tag> tempTag = new List<Tag>();


            tempTag = db.Tag.Where(a => a.personID == id).ToList();

            var temp = tempTag.Select(a => new Tag
            {
                nameTag = a.nameTag
            });


            return Json(temp, JsonRequestBehavior.AllowGet);
        }

        public string updateContact(Person contact, List<Phone> numbers, List<Email> emails, List<Tag> tags)
        {
            string message;

            if (emails != null && emails[0].emailAdress != null)
            {
                List<Email> tempEmail = new List<Email>(emails);
                if (tempEmail[0].emailAdress != null)
                {

                    foreach (var email in tempEmail)
                    {
                        var query = (from p in db.Email
                                     where p.emailAdress == email.emailAdress && p.personID != contact.personID
                                     select p.emailAdress).FirstOrDefault();
                        if (query != null)
                        {
                            return "ABORTED!!! Email adresa: " + email.emailAdress + "  existing";

                        }
                    }
                    var toDelete = db.Email.Where(a => a.personID == contact.personID).ToList();
                    if (toDelete.Any())
                    {
                        db.Email.RemoveRange(toDelete);
                        db.SaveChanges();

                    }
                    tempEmail.ForEach(a =>
                    {
                        a.personID = contact.personID;
                        db.Email.Add(a);
                        db.SaveChanges();
                    });
                }               

            }
            else
            {
                var toDelete = db.Email.Where(a => a.personID == contact.personID).ToList();
                if (toDelete.Any())
                {
                    db.Email.RemoveRange(toDelete);
                    db.SaveChanges();

                }

            }
            if (numbers !=null && numbers[0].phoneNumber != null)
            {               
                List<Phone> tempNumber = new List<Phone>(numbers);
                if (tempNumber[0].phoneNumber != null)
                {
                    
                    foreach (var number in tempNumber)
                    {
                        var query = (from p in db.Phone
                                     where p.phoneNumber == number.phoneNumber && p.personID != contact.personID
                                     select p.phoneNumber).FirstOrDefault();
                        if (query != null)
                        {
                            return "ABORTED!!! Phone number" + number.phoneNumber + " is existing";
                        }
                    }
                    var toDelete = db.Phone.Where(a => a.personID == contact.personID).ToList();
                    if (toDelete.Any())
                    {
                        db.Phone.RemoveRange(toDelete);
                        db.SaveChanges();

                    }

                    tempNumber.ForEach(a =>
                    {
                        a.personID = contact.personID;
                        db.Phone.Add(a);
                        db.SaveChanges();
                    });
                }
            }
            else
            {
                var toDelete = db.Phone.Where(a => a.personID == contact.personID).ToList();
                if (toDelete.Any())
                {
                    db.Phone.RemoveRange(toDelete);
                    db.SaveChanges();

                }

            }
            if (tags != null && tags[0].nameTag != null)
            {
                List<Tag> tempTags = new List<Tag>(tags);

                if (tempTags[0].nameTag != null)
                {
                    var toDelete = db.Tag.Where(a => a.personID == contact.personID).ToList();
                    db.Tag.RemoveRange(toDelete);
                    tempTags.ForEach(tag => {
                        tag.personID = contact.personID;
                        db.Tag.Add(tag);
                        db.SaveChanges();
                    });

                }                

            }
            else
            {
                var toDelete = db.Tag.Where(a => a.personID == contact.personID).ToList();
                if (toDelete.Any())
                {
                    db.Tag.RemoveRange(toDelete);
                    db.SaveChanges();

                }


            }
            if (contact.name != null)
            {
                db.Entry(contact).State = EntityState.Modified;
                db.SaveChanges();

                message = "succesfuly";
            }
            else
            {
                message = "error";
            }


            return message;
        }
    }
}