﻿var app = angular.module("ContactApp", ['ngRoute']);

app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
        when('/', {
            title: 'Start',
            templateUrl: '/Template/ContactMainPage.html',
            controller: 'contactListController'
        }).
            when('/EnterContact', {
                title: 'Unos',
                templateUrl: '/Template/EnterContact.html',
                controller: 'contactListController'
            }).
            when('/EditContact', {
                title: 'Edit',
                templateUrl: '/Template/EditContact.html',
                controller: 'contactListController'
            }).
            when('/BookmarkedContacts', {
                title: 'Favorite',
                templateUrl: '/Template/BookmarkedContacts.html',
                controller: 'contactListController'
            }).
            when('/DetailsContact', {
                title: 'Details',
                templateUrl: '/Template/DetailsContact.html',
                controller: 'contactListController'
            }).
            when('/SearchContacts', {
                title: 'Search',
                templateUrl: '/Template/PageSearchContacts.html',
                controller: 'contactListController'
            }).
        otherwise({
            redirectTo: '/'
        });
    }
]);


app.controller("contactListController", function ($scope, Data, $location) {

    $scope.contact = {
        Person: {
            personID: '',
            name: '',
            surname: '',
            adress: '',
            city: '',
            country: '',
            nickname: '',
            isBookmarked: ''
        },
        Tag: [{ nameTag: '' }],
        Phone: [{ phoneNumber: '' }],
        Email: [{ emailAdress: '' }]
    };

    $scope.searchValue = '';
    $scope.searchOption = {
        name: 0
    };

    //Dohvaća kontakte za ispis na početnoj strani
    $scope.getAllContacts=function(){
        getAllContacts();
    };

    getAllContacts = function () {
        Data.get('Contact/GetContacts').then(function (result) {
            $scope.contacts = result;
        });
    };

     getAllContacts();

    //označava da je kontakt stavljen na bookmark listu
    $scope.bookmarkContact = function (id) {
        Data.post("Contact/UpdateBookmark", {
            id: id
        })
    };
//omogućuju dodavanje proizvoljnog broja tagova i brisanje isto
    $scope.addEmailAdress = function () {

        $scope.contact.Email.push({});
    };

    $scope.addPhoneNumber = function () {
        $scope.contact.Phone.push({});
    };

    $scope.addNewTag = function () {
        $scope.contact.Tag.push({});
    };

    $scope.removeEmail = function (number) {
        $scope.contact.Email.splice(number, 1);
    };
    $scope.removePhoneNumber = function (number) {
        $scope.contact.Phone.splice(number, 1);
    };
    $scope.removeTag = function (number) {
        $scope.contact.Tag.splice(number, 1);
    };
    $scope.removeIfEmptyEmail = function (isEmpty, index) {
        if (isEmpty && $scope.contact.Email.length > 1 && $scope.contact.Email[0].emailAdress.length == 0)
            $scope.removeEmail(index);
    }
    $scope.removeIfEmptyPhone = function (isEmpty, index) {
        if (isEmpty && $scope.contact.Phone.length > 1 && $scope.contact.Phone[0].phoneNumber.length==0)
            $scope.removePhoneNumber(index);
    }
    $scope.removeIfEmptyTag = function (isEmpty, index) {
        if (isEmpty && $scope.contact.Tag.length > 1 && $scope.contact.Tag[0].nameTag.length == 0)
            $scope.removeTag(index);
    }

    //Unos novog kontakta
    $scope.enterNewContact = function () {
        Data.post('Contact/insertPerson', {
            contact: $scope.contact.Person,
            numbers: $scope.contact.Phone,
            emails: $scope.contact.Email,
            tags: $scope.contact.Tag
        }).then(function (result) {
            if (result != "success") {
                alert(result);
                getAllContacts();
            }
        });
        $location.path("/");
        
    };
    //brisanje kontakta
    $scope.deleteContact = function (id, index) {
        Data.post('Contact/deleteContact', {
            id: id
        }).then(function (result) {
            alert(result);
        });
        $scope.contacts.splice(index, 1);
    };
    //pretraživanje kontakata
    $scope.searchForContact = function () {
        Data.post('Contact/getSearchResult', {
            option: $scope.searchOption.name,
            searchValue: $scope.searchValue
        }).then(function (result) {
            if (result == "error")
                alert("Pogreška");
            else if (result.length == 0)
                alert("Traženi pojam nije pronaden");
            else
                $scope.searchResult = result;
        })
    }

    //Dobavlja detalje o kontaktu
    getDetailsOfContact = function (id) {

        Data.post('contact/editPerson', {
            id: id
        }).then(function (result) {
            changeStateOfEditPersonForm(result);
        });

        Data.post('contact/editEmails', {
            id: id
        }).then(function (result) {
            changeStateOfEditEmailsForm(result);
        });

        Data.post('contact/editNumbers', {
            id: id
        }).then(function (result) {
            changeStateOfEditNumbersForm(result);
        });

        Data.post('contact/editTags', {
            id: id
        }).then(function (result) {
            changeStateOfEditTagsForm(result);
        });
    };
    //Prebacuje view na template za editiranje te dobavlja podatke kojima će se ispuniti forma
    $scope.editContactFun = function (id) {

        $location.path('/EditContact');

        getDetailsOfContact(id);

    };
    //Prebacuje view na template za pregled korisnika te dobavlja podatke kojima će se ispuniti forma
    $scope.detailsOfContact = function (id) {
        $location.path('/DetailsContact');

        getDetailsOfContact(id);
    };
    //Dohvaca bookmarke
    $scope.getBookMarks = function () {
        Data.get('Contact/getBookmarked').then(function (result) {
            fillBookmarks(result);

        })
    };
    //ISPRAVAK M
    if ($location.path() == "/BookmarkedContacts")
    {
        $scope.getBookMarks();
    }

    $scope.bookmarkContact = function (id) {
        Data.post("contact/UpdateBookmark", {
            id: id
        })
    };

    fillBookmarks = function (result) {
        $scope.bookMarked = result;
    }
    //Dohvaća podatke zaprikaz na detaljima

    $scope.detailsOfContact = function (id) {
        $location.path('/DetailsContact');

        getDetailsOfContact(id);
    };

    //Dohvaća podatke zaprikaz na uređivanju
    $scope.editContactFun = function (id) {

        $location.path('/EditContact');

        getDetailsOfContact(id);

    };
    //dohvaća podatke
    getDetailsOfContact = function (id) {

        Data.post('contact/editPerson', {
            id: id
        }).then(function (result) {
            changeStateOfEditPersonForm(result);
        });

        Data.post('contact/editEmails', {
            id: id
        }).then(function (result) {
            changeStateOfEditEmailsForm(result);
        });

        Data.post('contact/editNumbers', {
            id: id
        }).then(function (result) {
            changeStateOfEditNumbersForm(result);
        });

        Data.post('contact/editTags', {
            id: id
        }).then(function (result) {
            changeStateOfEditTagsForm(result);
        });
    };

    changeStateOfEditPersonForm = function (result) {

        $scope.contact.Person = result[0];
    };

    changeStateOfEditEmailsForm = function (result) {
        if (result.length == 0) {
            $scope.contact.Email = [];
            $scope.addEmailAdress();
        }
        else {
            $scope.contact.Email = result;
        }
    };

    changeStateOfEditNumbersForm = function (result) {
        if (result.length == 0) {
            $scope.contact.Phone = [];
            $scope.addPhoneNumber();
        } else {
            $scope.contact.Phone = result;
        }
    };
    changeStateOfEditTagsForm = function (result) {
        if (result.length == 0) {
            $scope.contact.Tag = [];
            $scope.addNewTag();
        } else {
            $scope.contact.Tag = result;
        }
    };

    $scope.saveEditedContact = function () {

        if ($scope.contact.Email[0].length == 0)
            $scope.contact.Email.splice(0,1);

        if ($scope.contact.Phone[0].length == 0)
            $scope.contact.Phone.splice(0, 1);

        if ($scope.contact.Tag[0].length == 0)
            $scope.contact.Tag.splice(0, 1);

        Data.post('contact/updateContact', {
            contact: $scope.contact.Person,
            numbers: $scope.contact.Phone,
            emails: $scope.contact.Email,
            tags: $scope.contact.Tag
        }).then(function (result) {
            alert(result);
            getAllContacts();
        })

        $location.path("/");
    };

});
 //Rradi http zahtjeve i vraca rezultata
app.factory("Data", ['$http',
    function ($http) {

        var obj = {};
        obj.get = function (q) {
            return $http.get(q).then(function (results) {
                return results.data;
            });
        };
        obj.post = function (q, object) {
            return $http.post(q, object).then(function (results) {
                return results.data;
            });
        };

        return obj;
    }]);