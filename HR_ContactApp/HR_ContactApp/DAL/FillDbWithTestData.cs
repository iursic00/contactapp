﻿using HR_ContactApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HR_ContactApp.DAL
{
    public class FillDbWithTestData
    {
        //Klasa za punit bazu
        public static void Fill(ContactsEntities db)
        {
            List<Person> temp = new List<Person>();
            temp = db.Person.ToList();
            db.Person.RemoveRange(temp);
            db.SaveChanges();
            var persons = new List<Person>
                {
                    new Person { personID=1, name = "Ivan", surname="Uršić", adress="Vukovarska 33", country="Croatia",
                        nickname ="xy49f234",isBookmarked=true,
                        Email =new List<Email>() {
                            new Email {emailAdress="iursic00@fesb.hr" }
                        },
                        Phone =new List<Phone>() {
                            new Phone {phoneNumber="1234567"},
                            new Phone {phoneNumber="234567" }
                        },
                        Tag=new List<Tag>()
                        {
                            new Tag { Id=3,nameTag="ja"}
                        }
                    },

                    new Person { personID=2, name = "Mate", surname="Matić", adress = "Vladimir Nazor", country = "Croatia", nickname = "ohvtjjohdf",
                        Email =new List<Email>() {
                            new Email {emailAdress="mmatic@gmail.com"},
                            new Email {emailAdress="mmatic@yahooo.com"}
                        },
                        Phone =new List<Phone>() {
                            new Phone {phoneNumber="3456789"}
                        },
                        Tag=new List<Tag>()
                        {
                            new Tag { Id=1, nameTag="prijatelj"},
                            new Tag { Id=2,nameTag="zaposlen"},
                        }

                    }
                };
            persons.ForEach(person => db.Person.Add(person));
            db.SaveChanges();

        }

    }
}